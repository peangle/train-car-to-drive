import pickle
import pandas
import numpy
from PIL import Image
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils import shuffle

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.convolutional import Convolution2D
from keras.layers.pooling import MaxPooling2D, AveragePooling2D


def get_keras_model(number_of_labels):
    """
    Defines the model to predict steering angle based on input image.
    The model expects the image to be of size 32x96x3
    :param number_of_labels: The number of different steering angles to be output
    :return: A model that can be used to train and predict steering angle based on input image
    """
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, border_mode='same', input_shape=(32, 96, 3)))
    model.add(AveragePooling2D((2, 6), border_mode='same'))  # 16x16x32
    model.add(Activation('relu'))

    model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(Activation('relu'))  # 16x16x64
    model.add(Convolution2D(64, 3, 3, border_mode='same'))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 8x8x64
    model.add(Activation('relu'))

    model.add(Convolution2D(256, 3, 3, border_mode='same'))
    model.add(Activation('relu'))  # 4x4x256
    model.add(Convolution2D(256, 3, 3, border_mode='same'))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 4x4x256
    model.add(Activation('relu'))

    model.add(Flatten())
    model.add(Dense(4000))
    model.add(Activation('relu'))

    model.add(Dense(2000))
    model.add(Activation('relu'))

    model.add(Dense(number_of_labels))

    # Add dropout layer to avoid overfitting.
    model.add(Dropout(0.5))
    model.add(Activation('softmax'))

    return model


def normalize_data(data):
    """
    Normalize the input to have zero mean and one variance
    :param data: Image pixel data
    :return: Normalized image pixel data
    """
    return numpy.float32((data[:] - numpy.mean(data)) / numpy.std(data))


def get_valid_indices(images, angles):
    """
    Given the image and angles, returns the indices of the data to be used for training/validation
    It makes sure that there are not too many samples with steering angle 0 for training/validation
    It can also be modified to ignore certain images based on the timestamp if some particular data needs to be ignored
    :param images: Array of paths to images
    :param angles: Array of steering angles
    :return:
    """
    zeros = 0
    indices = []
    total_items = len(images)
    for i in range(total_items):
        if angles[i] == 0.0:
            zeros += 1
            if zeros > (total_items // 100):
                continue
        indices.append(i)
    return indices


def generate_data(center_images, steering_angles, valid_indices, number_of_labels, batch_size, is_training=True):
    """
    The generator to provide training data and validation data
    :param center_images:  Array of images
    :param steering_angles: Array of steering angles
    :param valid_indices: Array of valid indices to be considered
    :param number_of_labels: Total number of labels to map steering angles to
    :param batch_size: Size of batch
    :param is_training: True if being used for training False if being used for validation
    :return: A tuple of normalized images and onehot labels for them
    """
    while 1:
        if is_training:
            # Shuffle data in evey epoch
            valid_indices = shuffle(valid_indices)

        # Convert the steering angle to labels based on the input number_of_labels
        # Used 21 labels -1 to 1 at the interval of 0.1 maps to labels 0 to 21
        steering_angles = steering_angles[:] + 1
        steering_angles = steering_angles[:] * (number_of_labels // 2)
        steering_angles = steering_angles.astype(int)
        one_hot_labels = LabelBinarizer().fit_transform(steering_angles)
        if one_hot_labels.shape[1] < number_of_labels:
            zeros = numpy.zeros((one_hot_labels.shape[0], number_of_labels - one_hot_labels.shape[1]))
            one_hot_labels = numpy.c_[one_hot_labels, zeros]

        center_images = ["./driving_data/" + path[path.find("IMG"):] for path in center_images]
        center_images = [path.replace("\\", "/") for path in center_images]

        for batch in range(len(valid_indices) // batch_size):
            images = []
            batch_angles = []
            for i in valid_indices[batch * batch_size:(batch + 1) * batch_size]:
                path = center_images[i]
                with open(path, 'r+b') as f:
                    with Image.open(f) as image:
                        # Resize the image to about 1/3 of original size
                        image = image.resize((96, 48), Image.ANTIALIAS)
                        width = image.size[0]
                        height = image.size[1]
                        # Crop the top 1/3 as it's mostly the sky
                        image = image.crop((0, height//3, width, height))
                        image = numpy.array(image.convert("RGB"))
                        images.append(image)
                        batch_angles.append(one_hot_labels[i])
            yield (numpy.array([normalize_data(img) for img in images]),
                   numpy.array(batch_angles))


def train():
    batch_size = 32
    number_of_labels = 21
    driving_log = pandas.read_csv("./driving_data/driving_log.csv").as_matrix()
    train_labels = driving_log[:, 3]
    train_data = driving_log[:, 0]
    train_data, train_labels = shuffle(train_data, train_labels)

    # Split the available data in training and validation set
    validation_data_size = int(train_data.shape[0] * 0.2)
    validation_data = train_data[:validation_data_size, ...]
    validation_labels = train_labels[:validation_data_size]

    # Get valid indices for validation data
    validation_valid_indices = get_valid_indices(validation_data, validation_labels)
    validation_data_size = len(validation_valid_indices)

    train_data = train_data[validation_data_size:, ...]
    train_labels = train_labels[validation_data_size:]

    # Get valid indices for training data
    train_valid_indices = get_valid_indices(train_data, train_labels)

    # Total data points provided by the generator
    train_samples_per_epoch = (len(train_valid_indices) // batch_size) * batch_size
    validation_samples_per_epoch = (validation_data_size // batch_size) * batch_size

    model = get_keras_model(number_of_labels)
    print("Training...")

    # Using adam optimizer to deal with one less hyperparameter
    model.compile('adam', 'categorical_crossentropy', ['accuracy'])
    model.fit_generator(generate_data(train_data, train_labels, train_valid_indices, number_of_labels,
                                      batch_size),
                        validation_data=generate_data(validation_data, validation_labels,
                                                      validation_valid_indices, number_of_labels,
                                                      batch_size, False),
                        nb_val_samples=validation_samples_per_epoch,
                        samples_per_epoch=train_samples_per_epoch, nb_epoch=10)

    # Save the model
    model.save_weights('./model.h5')
    with open("./model.json", "w") as file:
        file.write(model.to_json())
    print("Model saved.")


train()
